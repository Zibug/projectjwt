package net.javaguides.springboot.dto;

import java.util.Set;

import net.javaguides.springboot.entity.Prodotto;

public class CategoriaDto {
	private Long id;
	private String img;
	private String nome;
	
	private Set<Prodotto> listaProdotti;
	
	public CategoriaDto() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Set<Prodotto> getListaProdotti() {
		return listaProdotti;
	}

	public void setListaProdotti(Set<Prodotto> listaProdotti) {
		this.listaProdotti = listaProdotti;
	}
	
	
	
}
