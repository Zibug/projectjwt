package net.javaguides.springboot.jwt;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter{
	
	private final AuthenticationManager authManager;
	
	public JwtAuthenticationFilter(AuthenticationManager authManager) {
		this.authManager = authManager;
	}

	//verifica autenticazione
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {
			UserDataRequest authenticationRequest = 
					new ObjectMapper()
					.readValue(request.getInputStream(), UserDataRequest.class);
			
			Authentication authentication = 
					new UsernamePasswordAuthenticationToken(
							authenticationRequest.getUsername(), 
							authenticationRequest.getPassword());
			
			//la varaibile authentication contiene una variabile authenticated = false
			Authentication verifyCredentials = authManager.authenticate(authentication); 
			//la varaibile authentication contiene una variabile authenticated = true
			return verifyCredentials; 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}
		
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		Algorithm key = Algorithm.HMAC256("chiave");
		//creo il token	
		String token = 
				JWT.create()
				.withIssuer(authResult.getName()) //subject
				
				.withClaim("authorities", authResult.getAuthorities().stream().map(element -> element.toString()).collect(Collectors.toList()))
				
				.withIssuedAt(new Date()) //date infos
				.withExpiresAt(java.sql.Date.valueOf(LocalDate.now().plusDays(2)))
				
				.sign(key); //key
		String role = 
				authResult
					.getAuthorities()
					.stream()
					.map(element -> element.toString())
					.filter(str -> str.startsWith("ROLE_"))
					.findFirst()
					.orElse("placeholder").replace("ROLE_", "");
		response.addHeader("Role", role);
		response.addHeader("Authorization", "Bearer " + token);
		response.addHeader("Access-Control-Expose-Headers", "Authorization, Role");
		
	}
}
