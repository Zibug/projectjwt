package net.javaguides.springboot.jwt;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.google.common.base.Strings;

public class VeryToken extends OncePerRequestFilter{

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		//verifica presenza del tokne nell'header o se presente, che inizi con Bearer
		String authorizationHeader = request.getHeader("Authorization");
		if(Strings.isNullOrEmpty(authorizationHeader) || !authorizationHeader.startsWith("Bearer ")) {
			
			//richiamo filterChain per passare request e response a successivi ulteriori filtri
			filterChain.doFilter(request, response);
			return;
		}
		
		//in caso passi la prima condizione, recuperiamo il token eliminando la parte "Bearer ":
		String token = authorizationHeader.replace("Bearer ", "");
		
		//verifica token
		try {
			Algorithm key = Algorithm.HMAC256("chiave");
			JWTVerifier verifier = JWT.require(key).build();
			DecodedJWT dec = verifier.verify(token);
		} catch (JWTVerificationException exception) {
			System.out.println(exception.getMessage());
		}
		
		DecodedJWT jwt = JWT.decode(token); //decodifica del token
		
		String issuer = jwt.getIssuer(); //getIssuer(username) acquisico lo username di chi fa la request

		Map<String, Claim> claims = jwt.getClaims(); //acqusizione del body dal token
			Set<SimpleGrantedAuthority> simGraAuth = 
					claims.get("authorities").asList(String.class)//acquisizione delle autorizzazioni dal body come lista di stringhe
						.stream()
						.map(element -> new SimpleGrantedAuthority(element)) //trasforamzione di ogni stringa in oggetto SimpleGrantedAuthority
						.collect(Collectors.toSet());

		//definizione del contesto attraverso un oggetto di tipo authentication che necessita del username e delle autorizzazioni
		Authentication authentication = new UsernamePasswordAuthenticationToken(issuer, null, simGraAuth);
			//ora il sistema sa chi è entrato e con quali autorizzazioni
			SecurityContextHolder.getContext().setAuthentication(authentication);
		
		filterChain.doFilter(request, response);
		
	}
	
	
}
