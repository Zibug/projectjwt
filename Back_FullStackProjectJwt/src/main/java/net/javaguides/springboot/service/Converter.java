package net.javaguides.springboot.service;

import org.springframework.stereotype.Component;

import net.javaguides.springboot.dto.CategoriaDto;
import net.javaguides.springboot.entity.Categoria;

@Component
public class Converter {
	
	public CategoriaDto entityToDtoCategoria(Categoria entity) {
		CategoriaDto dto = new CategoriaDto();
		dto.setId(entity.getId());
		dto.setNome(entity.getNome());
		dto.setImg(entity.getImg());
		dto.setListaProdotti(entity.getListaProdotti());
		return dto;
	}
	
	public Categoria dtoToEntityCategoria(CategoriaDto dto) {	
		Categoria entity = new Categoria();
		entity.setId(dto.getId());
		entity.setNome(dto.getNome());
		entity.setImg(dto.getImg());
		entity.setListaProdotti(dto.getListaProdotti());
		return entity;
	}
	
	
}
