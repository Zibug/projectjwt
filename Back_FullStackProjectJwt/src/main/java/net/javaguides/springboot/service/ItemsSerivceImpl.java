package net.javaguides.springboot.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import net.javaguides.springboot.dto.CategoriaDto;
import net.javaguides.springboot.entity.Categoria;
import net.javaguides.springboot.entity.Prodotto;
import net.javaguides.springboot.repository.CategoriaRepository;
import net.javaguides.springboot.repository.CategoriaRepositoryImpl;
import net.javaguides.springboot.repository.ProdottoRepository;

@Service
public class ItemsSerivceImpl implements ItemsService{
	
	
	@Autowired
	Converter converter;
	
	@Autowired
	ProdottoRepository prodottoRepo;
	
	private CategoriaRepository categoriaRepo;
	@Autowired
	public void IntemsServiceImpl(@Qualifier("mainRepository") CategoriaRepository categoriaRepo) {
		this.categoriaRepo = categoriaRepo;
	}
	
	@Override
	public CategoriaDto getCategoriaByIdService(Long id) {
		return converter.entityToDtoCategoria(categoriaRepo.getCategoriaByIdRepostiory(id));
	}

	@Override
	public CategoriaDto insertCategoriaService(CategoriaDto dto) {
		Categoria entity = converter.dtoToEntityCategoria(dto);
		return converter.entityToDtoCategoria(categoriaRepo.insertCategoriaRepository(entity));
	}

	@Override
	public List<CategoriaDto> getListaCategoriaService() {
		return categoriaRepo
					.getListCategoriaRepository()
					.stream()
					.map(entity -> converter
					.entityToDtoCategoria(entity))
					.collect(Collectors.toList());
	}

	@Override
	public CategoriaDto updateCategoriaService(CategoriaDto dto) {
		Categoria entity = converter.dtoToEntityCategoria(dto);
		Categoria result = categoriaRepo.updateCategoriaService(entity);
		return converter.entityToDtoCategoria(result);
	}

	@Override
	public void deleteCategoriaService(Long id) {
		categoriaRepo.deleteCategoriaRepository(id);
	}
	
//-------------------------------SERVICE PRODOTTO DA JPAREPOSITORY PRODOTTO----------------------------------
	@Override
	public List<Prodotto> getListaProdottoService() {
		return prodottoRepo.findAll();
		
	}
	
	@Override
	public Prodotto deleteProdottoService(Long id) {
		if(prodottoRepo.findById(id).isPresent()) {
			Prodotto entity = prodottoRepo.findById(id).get();
			prodottoRepo.delete(entity);
			return entity;
		} else {
			return null;
		}
	}
	
	@Override
	public Prodotto findProdottoByNameService(String nome) {
		Prodotto result = prodottoRepo.findByName(nome).stream().findFirst().get();
		return result;
	}

	@Override
	public Prodotto findProdottoByIdService(Long id) {
			return prodottoRepo.findById(id).get();
	}

	@Override
	public Prodotto insertProdottoService(Prodotto prodotto) {
		return prodottoRepo.save(prodotto);
	}

	@Override
	public Prodotto updateProdottoService(Prodotto prodotto) {
		return prodottoRepo.save(prodotto);
	}

	@Override
	public List<Prodotto> getListaProdottByCatId(Long id) {
		return prodottoRepo.findById_categoria(id);
	}

	
	

}