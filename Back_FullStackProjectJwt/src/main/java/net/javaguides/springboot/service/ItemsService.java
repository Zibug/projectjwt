package net.javaguides.springboot.service;

import java.util.List;

import net.javaguides.springboot.dto.CategoriaDto;
import net.javaguides.springboot.entity.Prodotto;

public interface ItemsService {
	public CategoriaDto getCategoriaByIdService(Long id);
	public List<CategoriaDto> getListaCategoriaService();
	public CategoriaDto insertCategoriaService(CategoriaDto dto);
	public CategoriaDto updateCategoriaService(CategoriaDto dto);
	public void deleteCategoriaService(Long id);
	
	public List<Prodotto> getListaProdottoService();
	public List<Prodotto> getListaProdottByCatId(Long id);
	public Prodotto deleteProdottoService(Long id);
	public Prodotto findProdottoByNameService(String nome);
	public Prodotto findProdottoByIdService(Long id);
	public Prodotto insertProdottoService(Prodotto prodotto);
	public Prodotto updateProdottoService(Prodotto prodotto);
}
