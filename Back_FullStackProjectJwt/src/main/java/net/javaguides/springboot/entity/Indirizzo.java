package net.javaguides.springboot.entity;

import javax.persistence.Embeddable;

@Embeddable
public class Indirizzo {
	private String via;
	private Integer numero;
	public Indirizzo() {
		
	}
	public String getVia() {
		return via;
	}
	public void setVia(String via) {
		this.via = via;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
	
	
}
