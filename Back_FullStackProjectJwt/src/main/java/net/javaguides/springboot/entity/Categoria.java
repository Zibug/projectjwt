package net.javaguides.springboot.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Categoria implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_categoria")
	private Long id;
	private String img;
	private String nome;
	
//--relazione unidirezionale-------------------
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_categoria")
	private Set<Prodotto> listaProdotti;
//----------------------------------------------
	
//--relazione bidirezionale------------------ INVERSE SIDE
//	@OneToMany(mappedBy = "categoria") //nome della proprieta nel Owner
//	private Set<Prodotto> listaProdotti;
//	
	
	//LISTE------------------------------------
	//possibile usare l'annotazione @OrderColumn(name = "<nomecolonna>") per creare un ulteriore
	//colonna che tenga conto dell'ordine di una proprieta List!!!
	//esiset anche @OrderBy per creare un ordine rispetto una certa colonna
	public Categoria() {
		
	}
	
	public Categoria(Long id, String img, String nome, Set<Prodotto> listaProdotti) {
		super();
		this.id = id;
		this.img = img;
		this.nome = nome;
		this.listaProdotti = listaProdotti;
	}
	
	public void addProdotto(Prodotto p) {
		this.listaProdotti.add(p);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Set<Prodotto> getListaProdotti() {
		return listaProdotti;
	}

	public void setListaProdotti(Set<Prodotto> listaProdotti) {
		this.listaProdotti = listaProdotti;
	}
	
	

	

}