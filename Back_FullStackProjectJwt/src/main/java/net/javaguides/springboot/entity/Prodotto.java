package net.javaguides.springboot.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class Prodotto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_prodotto")
	private Long id;
	private String img;
	private String name;
	
	private Long id_categoria;
	
//--Relazione Bidirezionale---------------------------------------------OWNER
//	@ManyToOne
//	@JoinColumn(name = "id_categoria") //la chiave esterna viene messa nel lato many
//	private Categoria categoria; 
	
	public Prodotto() {
		
	}	
	
	public Prodotto(Long id, String img, String name, Categoria categoria) {
		super();
		this.id = id;
		this.img = img;
		this.name = name;
//		this.categoria = categoria;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public Categoria getCategoria() {
//		return categoria;
//	}
//
//	public void setCategoria(Categoria categoria) {
//		this.categoria = categoria;
//	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Long getId_categoria() {
		return id_categoria;
	}

	public void setId_categoria(Long id_categoria) {
		this.id_categoria = id_categoria;
	}
	
	
	
	
}
