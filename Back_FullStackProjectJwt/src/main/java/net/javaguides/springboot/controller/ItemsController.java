package net.javaguides.springboot.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.dto.CategoriaDto;
import net.javaguides.springboot.entity.Categoria;
import net.javaguides.springboot.entity.Persona;
import net.javaguides.springboot.entity.Prodotto;
import net.javaguides.springboot.security.data.userDataDto.UserCredentialDto;
import net.javaguides.springboot.service.ItemsService;

@RestController
@RequestMapping("/items")
@CrossOrigin("http://localhost:4200")
public class ItemsController {
	
	@Autowired
	ItemsService itemsServ;
	
//--------------------CATEGORIES-----------------------------------------
	@GetMapping("/categories")
	public ResponseEntity<List<CategoriaDto>> getListCategoria(){
		List<CategoriaDto> listaCategoria = itemsServ.getListaCategoriaService();
		return new ResponseEntity<List<CategoriaDto>>(listaCategoria, HttpStatus.OK);
	}
	
	@GetMapping("/categories/{id}")
	public ResponseEntity<CategoriaDto> getCategoriaById(@PathVariable("id") Long id){
		CategoriaDto response = itemsServ.getCategoriaByIdService(id);
		return new ResponseEntity<CategoriaDto>(response, HttpStatus.OK);
	}
	
	@PostMapping("/categories")
	public ResponseEntity<CategoriaDto> inserCategoria(@RequestBody CategoriaDto categoria){
		CategoriaDto response = itemsServ.insertCategoriaService(categoria);
		return new ResponseEntity<CategoriaDto>(HttpStatus.CREATED);
	}
	
	@PutMapping("/categories")
	public ResponseEntity<CategoriaDto> updateCategoria(@RequestBody CategoriaDto categoria){
		CategoriaDto response = itemsServ.updateCategoriaService(categoria);
		return new ResponseEntity<CategoriaDto>(HttpStatus.OK);
	}
	
	@DeleteMapping("/categories/{id}")
	public ResponseEntity<?> deleteCategoria(@PathVariable("id") Long id){
		itemsServ.deleteCategoriaService(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
//----------------------PRODUCTS--------------------------------------
	@GetMapping("/prodotti")
	public ResponseEntity<List<Prodotto>> getListaProdotti(){
		List<Prodotto> listaProdotti = itemsServ.getListaProdottoService();
		return new ResponseEntity<List<Prodotto>>(listaProdotti, HttpStatus.OK);
	}
	
	@GetMapping("/prodotti/fbcid")
	public ResponseEntity<List<Prodotto>> getListaProdottByCatId(@RequestParam("idCat") Long id){
		List<Prodotto> listaProdotti = itemsServ.getListaProdottoService();
		return new ResponseEntity<List<Prodotto>>(listaProdotti, HttpStatus.OK);
	}
	
	@GetMapping("/prodotti/fbn")
	public ResponseEntity<Prodotto> findPrdoottoByName(@RequestParam("nome") String nome){
		Prodotto result = itemsServ.findProdottoByNameService(nome);
		return new ResponseEntity<Prodotto>(result, HttpStatus.OK);
	}
	
	@GetMapping("/prodotti/fbi")
	public ResponseEntity<Prodotto> findProdottoById(@RequestParam("id") Long id){
		return new ResponseEntity<Prodotto>(itemsServ.findProdottoByIdService(id), HttpStatus.OK);
	}
	
	@PostMapping("/prodotti")
	public ResponseEntity<Prodotto> insertProdotto(@RequestBody Prodotto prodotto){
		return new ResponseEntity<Prodotto>(itemsServ.insertProdottoService(prodotto), HttpStatus.CREATED);
	}
	
	@DeleteMapping("prodotti/d")
	public ResponseEntity<Prodotto> deleteProdotto(@RequestParam("id") Long id){
		return new ResponseEntity<Prodotto>(itemsServ.deleteProdottoService(id),HttpStatus.OK);
	}
	
	@PutMapping("prodotti")
	public ResponseEntity<Prodotto> inserProdotto(@RequestBody Prodotto prodotto){
		return new ResponseEntity<Prodotto>(itemsServ.updateProdottoService(prodotto), HttpStatus.OK);
	}
	
	
}