package net.javaguides.springboot.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.google.common.collect.Lists;

import net.javaguides.springboot.jwt.JwtAuthenticationFilter;
import net.javaguides.springboot.jwt.VeryToken;
import net.javaguides.springboot.security.data.UserPermissions;
import net.javaguides.springboot.security.data.UserRoles;
import net.javaguides.springboot.security.data.userDataService.UserService;

@Configuration
@EnableWebSecurity
public class ApplicationSecurity extends WebSecurityConfigurerAdapter {
	
	private final PasswordEncoder pswEncoder;
	private final UserService userService;
	
	@Autowired
	public ApplicationSecurity(PasswordEncoder pswEncoder, UserService userService) {
		this.pswEncoder = pswEncoder;
		this.userService = userService;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.csrf().disable()
				
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) //rende il sessione stateless
				.and()
				
				.addFilter(new JwtAuthenticationFilter(authenticationManager()))
				
				.addFilterAfter(new VeryToken(), UsernamePasswordAuthenticationFilter.class)
				
				.cors()
//	            .configurationSource(corsConfigurationSource())
	            
	            .and()
				
				.authorizeRequests()
				
				.antMatchers("/api").authenticated()
				
				.antMatchers(HttpMethod.POST, "/api/admin").hasAuthority(UserPermissions.ADMINWRITE.getPermission())
				.antMatchers(HttpMethod.PUT, "/api/admin").hasAuthority(UserPermissions.ADMINWRITE.getPermission())
				.antMatchers(HttpMethod.DELETE, "/api/admin").hasAuthority(UserPermissions.ADMINWRITE.getPermission())
				.antMatchers(HttpMethod.GET, "/api/admin/all").hasAnyAuthority(UserPermissions.ADMINGETALL.getPermission())
				.antMatchers(HttpMethod.GET, "/api/admin").hasAnyAuthority(UserPermissions.ADMINGET.getPermission())

				.antMatchers(HttpMethod.POST, "/api/users").hasAuthority(UserPermissions.USERWRITE.getPermission())
				.antMatchers(HttpMethod.PUT, "/api/users").hasAuthority(UserPermissions.USERWRITE.getPermission())
				.antMatchers(HttpMethod.DELETE, "/api/users").hasAuthority(UserPermissions.USERWRITE.getPermission())
				.antMatchers(HttpMethod.GET, "/api/users/all").hasAnyAuthority(UserPermissions.USERGETALL.getPermission())
				.antMatchers(HttpMethod.GET, "/api/users").hasAnyAuthority(UserPermissions.USERGET.getPermission())
				
				
				.antMatchers(HttpMethod.POST, "/items/**").hasAnyRole(UserRoles.AMMINISTRATORE.name(), UserRoles.SUBAMMINISTRATORE.name())
				.antMatchers(HttpMethod.PUT, "/items/**").hasAnyRole(UserRoles.AMMINISTRATORE.name(), UserRoles.SUBAMMINISTRATORE.name())
				.antMatchers(HttpMethod.DELETE, "/items/**").hasAnyRole(UserRoles.AMMINISTRATORE.name(), UserRoles.SUBAMMINISTRATORE.name())
				.antMatchers(HttpMethod.GET, "/items/**").permitAll()
				
				
				.antMatchers("/login").permitAll()
				.antMatchers("/**").permitAll()
				.and()
				.headers()
				.addHeaderWriter(new StaticHeadersWriter("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization"));
				
	}
	
	@Bean
	CorsConfigurationSource corsConfigurationSource() {
	    CorsConfiguration configuration = new CorsConfiguration();
	    configuration.setAllowedOrigins(Arrays.asList("*"));
	    configuration.setAllowedMethods(Arrays.asList("HEAD", "GET", "PUT", "POST", "DELETE"));
//	    configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
//	    configuration.setExposedHeaders(Arrays.asList("Authorization"));
	    configuration.setAllowCredentials(true);
	    //the below three lines will add the relevant CORS response headers
//	    configuration.addAllowedOrigin("localhost:4200");
	    configuration.addAllowedHeader("*");
	    configuration.addAllowedMethod("*");
	    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    source.registerCorsConfiguration("/**", configuration);
	    return source;
	}
	
//	@Bean
//	public WebMvcConfigurer corsConfigurer() {
//		return new WebMvcConfigurer() {
//			@Override
//			public void addCorsMappings(CorsRegistry registry) {
//				registry
//					.addMapping("/**")
//					.allowedOrigins("http://localhost:4200")
//					.allowedHeaders("Access-Control-Allow-Origin")
//					.allowedMethods("POST");
//			}
//		};
//	}
	
	//Definizione provider delle credenziali di autenticazione
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		//il metodo per la definizione dei provider accetta oggetti di tipo AuthenticationManagerBuilder, sfruttiamo una classe che la implementa, in questo caso sfrutto DaoAuthenticationProvider
		auth.authenticationProvider(daoAuthenticationProvider());
	}
	@Bean
	public DaoAuthenticationProvider daoAuthenticationProvider() {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		//al provider vanno definiti l'encoder e gli userdetails
		provider.setPasswordEncoder(pswEncoder);
		provider.setUserDetailsService(userService);
		return provider;
	}
}






