package net.javaguides.springboot.security.data.userDataService;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import net.javaguides.springboot.security.data.userDataDto.UserCredentialDto;
import net.javaguides.springboot.security.data.userDataEntity.UserCredentialEntity;
import net.javaguides.springboot.security.data.userDataRepository.UserRepository;
import net.javaguides.springboot.security.data.userDataRepository.UserRepositoryImpl;

@Service
public class UserService implements UserDetailsService{
	
	private final UserRepository userRep;
	@Autowired
	SecurityConverter converter;
	
	@Autowired
	public UserService(UserRepository userRep) {
		this.userRep = userRep;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		return userRep
				.selectApplicationUserByUsername(username) //per poter ritornare UserDetails e non Option<UserDetails> devo usare il metodo orElseThrow
				.orElseThrow(() -> new UsernameNotFoundException("Username " + username + " not found"));
	}
	
//--------------------------------------------------------------------------------------------------
	public List<UserCredentialDto> getUsersCredentials() {
		List<UserCredentialEntity> listaEntity = userRep.getUsersCredentials();
		List<UserCredentialDto> listaDto = listaEntity.stream().map(entity -> converter.entityToDto(entity)).collect(Collectors.toList());
		return listaDto;
	}
	
	public UserCredentialDto insertUserService(UserCredentialDto user) {
		System.out.println(user);
		UserCredentialEntity entity = converter.dtoToEntity(user);
		userRep.insertUserRepository(entity);
		return user;
	}
	
	public void deleteUserService(Long id) {
		this.userRep.deleteUserRepository(id);
	}
	
	public void updateUserService(UserCredentialDto user) {
		UserCredentialEntity entity = converter.dtoToEntity2(user);
		this.userRep.updateUserRepository(entity);
	}

}
