package net.javaguides.springboot.security.data;

public enum UserPermissions {
	ADMINGETALL("admin:getall"),
	ADMINGET("admin:get"),
	ADMINWRITE("admin:write"),
	USERGETALL("user:getall"),
	USERGET("user:get"),
	USERWRITE("user:write");
	
	private final String permission;
	
	private UserPermissions(String permission) {
		this.permission = permission;
	}
	
	public String getPermission() {
		return permission;
	}
}
