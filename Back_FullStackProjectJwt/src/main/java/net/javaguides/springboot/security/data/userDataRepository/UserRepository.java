package net.javaguides.springboot.security.data.userDataRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;

import net.javaguides.springboot.security.data.userDataEntity.UserCredentialEntity;

public interface UserRepository {
	public Optional<UserCredentialEntity> selectApplicationUserByUsername(String username);
	public List<UserCredentialEntity> getUsersCredentials();
	
	public UserCredentialEntity insertUserRepository(UserCredentialEntity user);
	public void deleteUserRepository(Long id);
	
	public void updateUserRepository(UserCredentialEntity user);
}
