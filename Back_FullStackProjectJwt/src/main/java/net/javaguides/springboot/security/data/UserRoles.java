package net.javaguides.springboot.security.data;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.google.common.collect.Sets;

public enum UserRoles {
	AMMINISTRATORE(Sets.newHashSet(UserPermissions.ADMINGETALL, UserPermissions.ADMINGET, UserPermissions.ADMINWRITE, UserPermissions.USERGETALL, UserPermissions.USERGET, UserPermissions.USERWRITE)),
	SUBAMMINISTRATORE(Sets.newHashSet(UserPermissions.ADMINGETALL, UserPermissions.ADMINGET, UserPermissions.USERGETALL, UserPermissions.USERGET, UserPermissions.USERWRITE)),
	UTENTE(Sets.newHashSet(UserPermissions.USERGET, UserPermissions.USERWRITE));
	
	private final Set<UserPermissions> permissions;
	
	private UserRoles(Set<UserPermissions> permissions) {
		this.permissions = permissions;
	}

	public Set<UserPermissions> getPermissions() {
		return permissions;
	}
	
	public Set<SimpleGrantedAuthority> getGrantedAuthorities(){
		Set<SimpleGrantedAuthority> authorities = getPermissions().stream().map(element -> new SimpleGrantedAuthority(element.getPermission())).collect(Collectors.toSet());
		authorities.add(new SimpleGrantedAuthority("ROLE_"+ this.name()));
		return authorities;
	}
	
}
