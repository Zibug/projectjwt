package net.javaguides.springboot.security.data.userDataControl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.entity.Persona;
import net.javaguides.springboot.security.data.userDataDto.UserCredentialDto;
import net.javaguides.springboot.security.data.userDataEntity.UserCredentialEntity;
import net.javaguides.springboot.security.data.userDataService.UserService;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:4200")
public class UserControl {
	
	private final UserService userService;
	
	@Autowired
	public UserControl(UserService userService) {
		this.userService = userService;
	}
	
	@GetMapping("/admin")
	public ResponseEntity<List<UserCredentialDto>> getTest() {
		List<UserCredentialDto> response = userService.getUsersCredentials();
		System.out.println("wat");
		return new ResponseEntity<List<UserCredentialDto>>(response, HttpStatus.OK);
	}
	
	@PostMapping("/admin")
	public ResponseEntity<String> insertUser(@RequestBody UserCredentialDto user) {
		userService.insertUserService(user);
		return new ResponseEntity<String>(user.getUsername(), HttpStatus.CREATED);
	}
	
	@PutMapping("/admin")
	public ResponseEntity<?> updateUser(@RequestBody UserCredentialDto user){
		userService.updateUserService(user);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@DeleteMapping("/admin/{id}")		
	public ResponseEntity<?> deleteUser(@PathVariable("id") Long id){
		this.userService.deleteUserService(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}