package net.javaguides.springboot.security.data.userDataRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.security.data.UserRoles;
import net.javaguides.springboot.security.data.userDataEntity.UserCredentialEntity;

@Repository
public class UserRepositoryImpl implements UserRepository {

	@PersistenceContext
	EntityManager entityManager;
	private final PasswordEncoder passwordEncoder;

	@Autowired
	public UserRepositoryImpl(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public Optional<UserCredentialEntity> selectApplicationUserByUsername(String username) {
		Optional<UserCredentialEntity> userFound = getUsersCredentials()
				.stream()
				.filter(userCredntial -> username.equals(userCredntial.getUsername()))
				.findFirst();
		return userFound;
	}

	@Override
	public List<UserCredentialEntity> getUsersCredentials() {
		Query q = entityManager.createQuery("SELECT u FROM UserCredentialEntity u");
		List<UserCredentialEntity> listaCredenziali = q.getResultList();
		listaCredenziali.stream().forEach(element -> element.setGrantedAuthority(element.getRole().getGrantedAuthorities()));
		listaCredenziali.stream().forEach(element -> System.out.println(element.getAuthorities()));
		listaCredenziali.add(
				new UserCredentialEntity( //account predefinito di gestione
						"admin", 
						passwordEncoder.encode("password1"), 
						"mail@mail.com",
						UserRoles.AMMINISTRATORE,
						true, 
						true, 
						true, 
						true
				)
		);
		return listaCredenziali;
	}

	@Override
	@Transactional
	public UserCredentialEntity insertUserRepository(UserCredentialEntity user) {
		try {
			entityManager.persist(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		return user;
	}

	@Override
	@Transactional
	public void deleteUserRepository(Long id) {
		UserCredentialEntity entity = entityManager.find(UserCredentialEntity.class, id);
		entityManager.remove(entity);
	}

	@Override
	@Transactional
	public void updateUserRepository(UserCredentialEntity user) {
		entityManager.merge(user);
		
	}



}
