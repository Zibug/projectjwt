package net.javaguides.springboot.security.data.userDataService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import net.javaguides.springboot.security.data.PassEncoder;
import net.javaguides.springboot.security.data.userDataDto.UserCredentialDto;
import net.javaguides.springboot.security.data.userDataEntity.UserCredentialEntity;

@Component
public class SecurityConverter {
	
	PasswordEncoder passEncoder;
	
	@Autowired
	public SecurityConverter(PasswordEncoder passEncoder) {
		this.passEncoder = passEncoder;
	}
	
	public UserCredentialEntity dtoToEntity(UserCredentialDto userDto) {

		UserCredentialEntity userEntity = new UserCredentialEntity();

		userEntity.setId(userDto.getId());
		userEntity.setUsername(userDto.getUsername());
		userEntity.setPassword(passEncoder.encode(userDto.getPassword()));
		userEntity.setMail(userDto.getMail());

		userEntity.setAccountNonExpired(userDto.isAccountNonExpired());
		userEntity.setAccountNonLocked(userDto.isAccountNonLocked());
		userEntity.setCredentialsNonExpired(userDto.isCredentialsNonExpired());
		userEntity.setEnabled(userDto.isEnabled());

		userEntity.setRole(userDto.getRole());
		
		userEntity.setGrantedAuthority(userEntity.getRole().getGrantedAuthorities());

		return userEntity;
	}
	
	public UserCredentialEntity dtoToEntity2(UserCredentialDto userDto) {

		UserCredentialEntity userEntity = new UserCredentialEntity();

		userEntity.setId(userDto.getId());
		userEntity.setUsername(userDto.getUsername());
		userEntity.setPassword(userDto.getPassword());
		userEntity.setMail(userDto.getMail());

		userEntity.setAccountNonExpired(userDto.isAccountNonExpired());
		userEntity.setAccountNonLocked(userDto.isAccountNonLocked());
		userEntity.setCredentialsNonExpired(userDto.isCredentialsNonExpired());
		userEntity.setEnabled(userDto.isEnabled());

		userEntity.setRole(userDto.getRole());
		
		userEntity.setGrantedAuthority(userEntity.getRole().getGrantedAuthorities());

		return userEntity;
	}

	public UserCredentialDto entityToDto(UserCredentialEntity userEntity) {

		UserCredentialDto userDto = new UserCredentialDto();

		userDto.setId(userEntity.getId());
		userDto.setUsername(userEntity.getUsername());
		userDto.setMail(userEntity.getMail());
		userDto.setPassword(userEntity.getPassword());

		userDto.setAccountNonExpired(userEntity.isAccountNonExpired());
		userDto.setAccountNonLocked(userEntity.isAccountNonLocked());
		userDto.setCredentialsNonExpired(userEntity.isCredentialsNonExpired());
		userDto.setEnabled(userEntity.isEnabled());

		userDto.setRole(userEntity.getRole());

		return userDto;
	}
}
