package net.javaguides.springboot.repository;

import java.util.List;

import net.javaguides.springboot.entity.Categoria;

public interface CategoriaRepository {
	public List<Categoria> getListCategoriaRepository();
	public Categoria insertCategoriaRepository(Categoria entity);
	public void deleteCategoriaRepository(Long id);
	public Categoria getCategoriaByIdRepostiory(Long id);
	public Categoria updateCategoriaService(Categoria entity);
}
