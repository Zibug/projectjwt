package net.javaguides.springboot.repository;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import net.javaguides.springboot.entity.Categoria;

@Repository("mainRepository")
public class CategoriaRepositoryImpl implements CategoriaRepository{
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<Categoria> getListCategoriaRepository() {
		Query q = entityManager.createQuery("SELECT u FROM Categoria u");
		return q.getResultList();
	}

	@Override
	@Transactional
	public Categoria insertCategoriaRepository(Categoria entity) {
		entityManager.persist(entity);
		return entity;
	}

	@Override
	@Transactional
	public void deleteCategoriaRepository(Long id) {
		Categoria entity = entityManager.find(Categoria.class, id);
		entityManager.remove(entity);
		return;
	}

	@Override
	public Categoria getCategoriaByIdRepostiory(Long id) {
		Categoria entity = entityManager.find(Categoria.class, id);
//		Query q = entityManager.createQuery("SELECT u FROM Categoria u WHERE u.id = :id");
//		List<Categoria> listEntity = q.setParameter("id", id).getResultList();
//		listEntity.stream().forEach(element -> element.getListaProdotti().stream().forEach(thing -> System.out.println(thing.getId())));
//		Categoria entity = listEntity.stream().findFirst().orElseThrow();
		return entity;
	}

	@Override
	@Transactional
	public Categoria updateCategoriaService(Categoria entity) {
		entityManager.merge(entity);
		return entity;
	}
}