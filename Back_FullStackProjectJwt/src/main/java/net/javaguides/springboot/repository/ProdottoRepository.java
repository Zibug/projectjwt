package net.javaguides.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.entity.Prodotto;

@Repository("prodRepository")
public interface ProdottoRepository extends JpaRepository<Prodotto, Long>{
	//possibilita di introddure un metodo per la ricerca di un oggetto data una variabile gia
	//inclusa nei metodi di jpa
	List<Prodotto> findByName(String name); 
	@Query("SELECT u FROM Prodotto u WHERE u.id_categoria = ?1")
	List<Prodotto> findById_categoria(Long id_categoria);
//	List<Prodotto> findById_categoria(Long id_categoria);
}
