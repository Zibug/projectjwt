import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from '../index/index.component';
import { LoginComponent } from '../login/login.component';
import { DataManagementComponent } from '../data-management/data-management.component';
import { AdminDataManagementComponent } from '../data-management/admin-data-management/admin-data-management.component';
import { UserDataManagementComponentComponent } from '../data-management/user-data-management-component/user-data-management-component.component';
import { FormDataManagementComponent } from '../data-management/form-data-management/form-data-management.component';
import { CategorieDataManagementComponent } from '../data-management/categorie-data-management/categorie-data-management.component';
import { ProdottiDataManagementComponent } from '../data-management/prodotti-data-management/prodotti-data-management.component';


const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "index"
  },
  {
    path: "index",
    component: IndexComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "dm",
    component: DataManagementComponent,
    children:
      [
        {
          path: "admin",
          component: AdminDataManagementComponent
        },
        {
          path: "user",
          component: UserDataManagementComponentComponent
        },
        {
          path: "form/:id",
          component: FormDataManagementComponent
        },
        {
          path: "categorie",
          component: CategorieDataManagementComponent,
        },
        {
          path: "prodotti/:id",
          component: ProdottiDataManagementComponent

        }
      ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRountingModule { }
