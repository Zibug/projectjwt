import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { IndexComponent } from './index/index.component';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AppRountingModule } from './app-rounting/app-rounting.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataManagementComponent } from './data-management/data-management.component';
import { AdminDataManagementComponent } from './data-management/admin-data-management/admin-data-management.component';
import { UserDataManagementComponentComponent } from './data-management/user-data-management-component/user-data-management-component.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormDataManagementComponent } from './data-management/form-data-management/form-data-management.component';
import { CategorieDataManagementComponent } from './data-management/categorie-data-management/categorie-data-management.component';
import { ProdottiDataManagementComponent } from './data-management/prodotti-data-management/prodotti-data-management.component';
import { UserDataService } from './auth/services/user-data.service';
import { CategorieProdottiService } from './data/services/categorie-prodotti.service';
import { ProdottiService } from './data/services/prodotti.service';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    LoginComponent,
    DataManagementComponent,
    AdminDataManagementComponent,
    UserDataManagementComponentComponent,
    FormDataManagementComponent,
    CategorieDataManagementComponent,
    ProdottiDataManagementComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    RouterModule,
    AppRountingModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  providers: [UserDataService, CategorieProdottiService, ProdottiService],
  bootstrap: [AppComponent]
})
export class AppModule { }