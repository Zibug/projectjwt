import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorieDataManagementComponent } from './categorie-data-management.component';

describe('CategorieDataManagementComponent', () => {
  let component: CategorieDataManagementComponent;
  let fixture: ComponentFixture<CategorieDataManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategorieDataManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorieDataManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
