import { Component, OnInit } from '@angular/core';
import { CategorieProdottiService } from '../../data/services/categorie-prodotti.service';
import { Categoria } from '../../data/interfaces/Categorie';
import { faTrashAlt, faEdit, faList } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-categorie-data-management',
  templateUrl: './categorie-data-management.component.html',
  styleUrls: ['./categorie-data-management.component.css']
})
export class CategorieDataManagementComponent implements OnInit {

  listaCat:Categoria[];
  deleteIcon = faTrashAlt;
  editIcon = faEdit;
  showProd = faList;

  constructor(private serviceCat:CategorieProdottiService) { }

  ngOnInit(): void {
    $(function(){
      $("table").animate({opacity: "1"})
    });
    this.serviceCat.getListCategoria().subscribe(
      (response) => {
        this.listaCat = response
      }
    )
  }

  _deleteCat(){

  }
}
