import { Router } from '@angular/router';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserData } from 'src/app/auth/classes/UserData';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { UserDataService } from '../../auth/services/user-data.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-form-data-management',
  templateUrl: './form-data-management.component.html',
  styleUrls: ['./form-data-management.component.css']
})
export class FormDataManagementComponent implements OnInit {
  @Input() user: UserData;
  closeIcon = faWindowClose;
  formData: FormGroup;
  newUser: boolean;
  @Output() showEditForm: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private fb: FormBuilder, private service: UserDataService, private route: Router) { }

  ngOnInit(): void {
    if (this.user.id == -1) {
      this.newUser = true;
    } else {
      this.newUser = false;
    }
    this.formData = this.fb.group(
      {
        username: [this.user.username, Validators.required],
        password: [this.user.password, [Validators.required, Validators.pattern("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,15}")]],
        mail: [this.user.mail, [Validators.required]],
        role: [this.user.role, Validators.required],
        enabled: [this.user.enabled, Validators.required]
      }
    )

    this.formData.valueChanges.subscribe(
      (data) => {
        this.user.username = data.username
        this.user.password = data.password
        this.user.mail = data.mail
        this.user.role = data.role
        this.user.enabled = data.enabled
      }
    )
  }

  public _showEditForm() {
    this.showEditForm.emit(false)
  }

  public _onAdd() {
    this.service.insertUser(this.user).pipe(
      map(
        () => {
            this._showEditForm()
        }
      )
    ).subscribe(
      () => {
        this.route.navigate(["dm"]).then(
          () => {
            this.route.navigate(["dm", "admin"])
          }
        )
      }
    );
  }
  public _onEdit() {
    this.service.updateUser(this.user).pipe(
      map(
        () => {
            this._showEditForm()
        }
      )
    ).subscribe(
      () => {
        this.route.navigate(["dm"]).then(
          () => {
            this.route.navigate(["dm", "admin"])
          }
        )
      }
    );
  }
}
