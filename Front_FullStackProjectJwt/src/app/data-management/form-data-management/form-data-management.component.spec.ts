import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDataManagementComponent } from './form-data-management.component';

describe('FormDataManagementComponent', () => {
  let component: FormDataManagementComponent;
  let fixture: ComponentFixture<FormDataManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormDataManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDataManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
