import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Prodotto } from 'src/app/data/interfaces/Prodotto';
import { ProdottiService } from 'src/app/data/services/prodotti.service';

@Component({
  selector: 'app-prodotti-data-management',
  templateUrl: './prodotti-data-management.component.html',
  styleUrls: ['./prodotti-data-management.component.css']
})
export class ProdottiDataManagementComponent implements OnInit {
  idCat:number;
  listaProdotti:Prodotto[]

  constructor(private actRoute:ActivatedRoute, private service:ProdottiService) { }

  ngOnInit(): void {
    this.actRoute.params.subscribe(
      (parametri) => {//observable
        if(parametri.id != -1){
          this.idCat = parametri.id;
          this.service.getProdottiByCatId(this.idCat).subscribe(
            (response)=>{
              this.listaProdotti = response
            }
          );
        } else {
          this.service.getListaProdotti().subscribe(
            response => {
              this.listaProdotti = response
            }
          )
        }
      }
    )
    

  }

  

  
}
