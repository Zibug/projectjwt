import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdottiDataManagementComponent } from './prodotti-data-management.component';

describe('ProdottiDataManagementComponent', () => {
  let component: ProdottiDataManagementComponent;
  let fixture: ComponentFixture<ProdottiDataManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProdottiDataManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdottiDataManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
