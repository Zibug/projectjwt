import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UserDataService } from '../../auth/services/user-data.service';
import { UserDataInt } from '../../auth/interfaces/UserDataInt';
import { faCheck, faCross, faEdit, faMinus } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { UserData } from 'src/app/auth/classes/UserData';

@Component({
  selector: 'app-admin-data-management',
  templateUrl: './admin-data-management.component.html',
  styleUrls: ['./admin-data-management.component.css']
})
export class AdminDataManagementComponent implements OnInit {

  userData: UserDataInt[];
  statusIcon;
  statusIconColor: string;
  editIcon = faEdit;
  deleteIcon = faMinus;
  showEdit: boolean = false;
  userEdit: UserData;

  constructor(private service: UserDataService, private route: Router) { }

  ngOnInit(): void {
    if (sessionStorage.getItem("Role") != undefined) {
      if (sessionStorage.getItem("Role") == "AMMINISTRATORE" || sessionStorage.getItem("Role") == "SUBAMMINISTRATORE") {
        this.service.getUserList().subscribe(
          (response) => {
            this.userData = response
          }
        ) 
      }
      $(function(){
        $(".table").animate({opacity: 1})
      });
    }
  }

  public status(stato: boolean) {
    if (stato) {
      this.statusIcon = faCheck
      this.statusIconColor = "green"
    } else {
      this.statusIcon = faCross
      this.statusIconColor = "red"
    }
  }

  public delete(id: number) {
    console.log(id)
    if (id != null) {
      this.service.deleteUser(id).subscribe(
        () => {
          this.route.navigate(["dm"]).then(
            () => this.route.navigate(["dm", "admin"])
          )
        }
      )
    }
  }

  public _showEdit(show?:boolean, user:UserData = new UserData()) {
    this.showEdit = show
    this.userEdit = user
  }

  public _accTypeAdmin(){
    if(sessionStorage.getItem("Role") == "UTENTE"){
      return false;
    } else {
      return true;
    }
  }
}

