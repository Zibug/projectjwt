import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-management',
  templateUrl: './data-management.component.html',
  styleUrls: ['./data-management.component.css']
})
export class DataManagementComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


  public _accTypeAdmin(){
    if(sessionStorage.getItem("Role") == "UTENTE"){
      return false
    } else {
      return true
    }
  }
}
