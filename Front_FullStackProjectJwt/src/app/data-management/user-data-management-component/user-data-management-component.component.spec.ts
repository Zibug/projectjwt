import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDataManagementComponentComponent } from './user-data-management-component.component';

describe('UserDataManagementComponentComponent', () => {
  let component: UserDataManagementComponentComponent;
  let fixture: ComponentFixture<UserDataManagementComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserDataManagementComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDataManagementComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
