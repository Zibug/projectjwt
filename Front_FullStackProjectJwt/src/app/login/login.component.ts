import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserAndPsw } from '../auth/classes/UserAndPsw';
import { UserDataService } from '../auth/services/user-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;
  user:UserAndPsw = new UserAndPsw();
  token:String = "";
  failedAuth:boolean = false;
  loginStatus:boolean = false;

  constructor(private fb:FormBuilder, private service:UserDataService, private route:Router) { }

  ngOnInit(): void {
    if(sessionStorage.getItem("token") != undefined){
      this.loginStatus = true;
    }
    this.loginForm = this.fb.group(
      {
        username : ["", Validators.required],
        password : ["", Validators.required]
      }
    )
    this.loginForm.valueChanges.subscribe(
      value => (
        this.user.username = value.username,
        this.user.password = value.password
      )
    )
  }

  public onSub(){
    this.service.authentication(this.user).subscribe(
      () => {
          this.route.navigate([""])
          this.loginStatus = false;
      },
      (error) =>{
        this.failedAuth = true;
      }
    );
  }

}
