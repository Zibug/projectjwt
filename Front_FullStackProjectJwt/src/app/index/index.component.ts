import { Component, OnInit } from '@angular/core';  
import { UserDataService } from '../auth/services/user-data.service';
import { Router } from '@angular/router';
import { CategorieProdottiService } from '../data/services/categorie-prodotti.service';
import { Categoria } from '../data/interfaces/Categorie';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  loginStatus:boolean;
  username:string = sessionStorage.getItem("username");
  listaCat:Categoria[];

  parola:string = "parola";

  // parola:string = (0>1) ? "maggiore" : "minore";

  get getService(){
    return this.service;
  }

  constructor(private service:UserDataService, private route:Router, private prodService:CategorieProdottiService) {

  }

  ngOnInit(): void {
    this.service = new UserDataService()
    if(sessionStorage.getItem("token")){
      this.loginStatus = true
    }
    else{
      this.loginStatus = false
    }
    this.prodService.getListCategoria().subscribe(
      (response) => {
        this.listaCat = response;
      }
    )
    let obj = {
      "var1" : "var1",
      "var2" : "var2"
    }
  }

  public logOut(){
    this.loginStatus = false;
    this.service.token = ""
    sessionStorage.removeItem("token")
    sessionStorage.removeItem("username")
    sessionStorage.removeItem("Role")
    // this.service.username = ""
    this.route.navigate([""])
  }

  public dataManagementRoute(){
    switch(sessionStorage.getItem("Role")){
      case "AMMINISTRATORE":
        this.route.navigate(["dm", "admin"])
        break;
      case "SUBAMMINISTRATORE":
        this.route.navigate(["dm", "admin"])
        break;
      case "UTENTE":
        this.route.navigate(["dm", "user"])
        break;
    }
    
  }
}
