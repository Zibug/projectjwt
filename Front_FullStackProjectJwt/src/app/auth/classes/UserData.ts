import { UserDataInt } from '../interfaces/UserDataInt';
export class UserData implements UserDataInt{
    id: number = -1;
    username: string = "";
    password: string = "";
    mail: string = "";
    accountNonExpired: boolean = true;
    accountNonLocked: boolean = true;
    credentialsNonExpired: boolean = true;
    enabled: boolean = true;
    role: string = "role";
}