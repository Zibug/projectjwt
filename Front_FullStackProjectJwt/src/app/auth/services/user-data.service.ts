import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserAndPswInt } from '../interfaces/UserAndPswInt';
import { map } from 'rxjs/operators'
import { UserDataInt } from '../interfaces/UserDataInt';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  url: string = "http://localhost:8080/";

  token: string = sessionStorage.getItem("token");

  constructor(private http?: HttpClient) { }

  public authentication(userData: UserAndPswInt): Observable<any> {
    return this.http.post<any>(`${this.url}login`, userData, { observe: 'response' })
      .pipe(
        map(
          response => {
            this.token = response.headers.get("Authorization")
            this.token = this.token.replace("Bearer ", "")
            sessionStorage.setItem("token", this.token)
            sessionStorage.setItem("username", userData.username)
            sessionStorage.setItem("Role", response.headers.get("Role"))
          }
        )
      );
  }

  public getUserList(): Observable<UserDataInt[]> {
    return this.http.get<UserDataInt[]>(`${this.url}api/admin`,
      {
        headers: { "Authorization": `Bearer ${sessionStorage.getItem("token")}` }
      }
    )
  }

  public deleteUser(id: number): Observable<void> {
    return this.http.delete<void>(`${this.url}api/admin/${id}`,
      {
        headers: { "Authorization": `Bearer ${sessionStorage.getItem("token")}` }
      }
    );
  }

  public insertUser(userData: UserDataInt): Observable<string> {
    delete userData.id
    return this.http.post<string>(`${this.url}api/admin`, userData,
      {
        headers: { "Authorization": `Bearer ${sessionStorage.getItem("token")}` }
      }
    );
  }

  public updateUser(userData: UserDataInt): Observable<void> {
    return this.http.put<void>(`${this.url}api/admin`, userData,
      {
        headers: { "Authorization": `Bearer ${sessionStorage.getItem("token")}` }
      }
    );
  }
}