export interface UserDataInt{
    id:number;
    username:string;
    password:string;
    mail:string;
    accountNonExpired:boolean;
    accountNonLocked:boolean;
    credentialsNonExpired:boolean;
    enabled:boolean;
    role:string;
}

// private Long id;
// private String username;
// private String mail;
// private String password;
// private boolean isAccountNonExpired;
// private boolean isAccountNonLocked;
// private boolean isCredentialsNonExpired;
// private boolean isEnabled;
// private UserRoles role;